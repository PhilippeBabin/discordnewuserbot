package NewUserBot.NewUserBot;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;

import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;

public class Main extends ListenerAdapter {
    static String token = "";
    static String roleName = "";
    public static void main(String[] args) {
        
        Scanner userInput = new Scanner(System.in);
        if (args.length < 1) {
            System.out.println("You didn't provide an argument when you launched the app!");
            System.out.println("Last chance, enter the bot token now: ");
            token = userInput.next();
        } else {
            token = args[0];
        }
        try {
            JDABuilder.create(token, GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_PRESENCES, GatewayIntent.GUILD_MESSAGES)
                    .addEventListeners(new Main())
                    .setActivity(Activity.listening("you."))
                    .build();
        } catch (Exception e) {
            System.out.println("Token isn't correct! Please re-launch the bot with a correct token.");
            System.exit(1);
        }
        userInput.close();
    }
    
    private Main() {
        return;
    }
    
    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
    	if (event.getUser().isBot())
    	{
    		return;
    	}
        Role role = null;
        Guild guild = event.getGuild();
        TextChannel defaultChannel = event.getGuild().getDefaultChannel();
        try {
        	Long roleId = getRoleWithId(guild.getIdLong());
            role = event.getGuild().getRoleById(roleId);
        } catch (Exception e) {
            defaultChannel.sendMessage("Role doesn't exist!").queue();
            System.exit(1);
        }
        guild.addRoleToMember(event.getMember().getIdLong(), role).queue();
     }
    
    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event)
    {
        if (!event.getAuthor().isBot())
        {
            User adminUser = event.getAuthor();
            for (Permission perm : event.getGuild().getMember(adminUser).getPermissions())
            {
                if (perm == Permission.ADMINISTRATOR)
                {
                    saveNewUserRole(event.getMessage());
                    break;
                }
            }
        }
    }
    
    private void saveNewUserRole(Message message)
    {
        Long guildId = message.getGuild().getIdLong();
        String[] command = message.getContentRaw().split(" ", 2);
        if ("/newRole".equals(command[0]) && command.length == 2) {
            try
            {
                File file = new File("NewUserDatabase.txt");
                if (!file.exists())
                {
                    Files.createFile(file.toPath());
                }
                checkAndRemoveLine(file, guildId);
                FileWriter writer = new FileWriter(file, true);
                writer.write(message.getGuild().getIdLong() + "°" + command[1] + "\n");
                writer.close();
                message.getChannel().sendMessage("Role sauvegardé.").queue();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
    
    private void checkAndRemoveLine(File file, Long guildId)
    {
    	try
    	{
    		List<String> lines = FileUtils.readLines(file);
    		List<String> updatedLines = lines.stream().filter(s -> !s.contains(Long.toString(guildId))).collect(Collectors.toList());
    		FileUtils.writeLines(file, updatedLines, false);
    	} catch (Exception e)
    	{
    		e.printStackTrace();
    	}
    }
    
    private Long getRoleWithId(Long guildId)
    {
        try
        {
            File myObj = new File("NewUserDatabase.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine())
            {
                String data = myReader.nextLine();
                if (data.contains(Long.toString(guildId)))
                {
                	myReader.close();
                	return Long.parseLong(data.split("°")[1]);
                }
            }
            myReader.close();
        }
        catch (FileNotFoundException e)
        {
            System.out.println("An error occurred.");
            e.printStackTrace();
            return null;
        }
		return guildId;
    }
}
